﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Controls;

namespace MvvmCalculator
{
    public partial class MainPage
    {
        public MainPage()
        {
            this.DataContext = new MainViewModel(this);

            InitializeComponent();
        }
    }

    public class MainViewModel
    {
        private readonly PhoneApplicationPage _page;

        public List<ButtonsItems> Items { get; private set; }
        public RelayCommand<ButtonsItems> ButtonClickCommand { get; private set; }

        public MainViewModel(PhoneApplicationPage page)
        {
            _page = page;

            this.ButtonClickCommand = new RelayCommand<ButtonsItems>(this.ButtonClickHandler);

            this.Items = new List<ButtonsItems>
                {
                    new ButtonsItems
                        {
                            Name = "WithCodeBehind",
                            Text = "App With Code Behind",
                            Url = "/CalculatorTraditional.xaml",
                            ButtonClickCommand = this.ButtonClickCommand
                        },
                    new ButtonsItems
                        {
                            Name = "WithNoCodeBehind",
                            Text = "App With No Code Behind",
                            Url = "/CalculatorNoCodeBehind.xaml",
                            ButtonClickCommand = this.ButtonClickCommand
                        },
                    new ButtonsItems
                        {
                            Name = "WithTypedRelayCommand",
                            Text = "App With Typed RelayCommand",
                            Url = "/CalculatorWithTypedRelayCommand.xaml",
                            ButtonClickCommand = this.ButtonClickCommand
                        },
                    new ButtonsItems
                        {
                            Name = "WithViewModelReference",
                            Text = "App With Traditional ViewModel",
                            Url = "/CalculatorTraditionalViewModel.xaml",
                            ButtonClickCommand = this.ButtonClickCommand
                        },
                    new ButtonsItems
                        {
                            Name = "CalculatorRefactoredViewModel",
                            Text = "App With Decoupled ViewModels",
                            Url = "/CalculatorRefactoredViewModel.xaml",
                            ButtonClickCommand = this.ButtonClickCommand
                        },
                    new ButtonsItems
                        {
                            Name = "CalculatorAutomaticViewModel",
                            Text = "App With Comm. Between ViewModels",
                            Url = "/CalculatorAutomaticViewModel.xaml",
                            ButtonClickCommand = this.ButtonClickCommand
                        }
                };
        }

        private void ButtonClickHandler(ButtonsItems item)
        {
            _page.NavigationService.Navigate(new Uri(item.Url, UriKind.Relative));
        }
    } 

    public class ButtonsItems
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
        public RelayCommand<ButtonsItems> ButtonClickCommand { get; set; } 
    }
}