﻿using System;
using System.Runtime.Serialization;

namespace MvvmCalculator.Core.Models
{
    [DataContract]
    public class CalculationResult
    {
        [DataMember]
        public double Left { get; set; }

        [DataMember]
        public double Right { get; set; }

        [DataMember]
        public string Operation { get; set; }

        [DataMember]
        public double Result { get; set; }

        public CalculationResult()
        {

        }

        public CalculationResult(double left, double right, double result, string operation)
        {
            this.Left = left;
            this.Right = right;

            //Shorten to two digits
            this.Result = Math.Round(result, 2);

            this.Operation = operation;
        }
    }
}