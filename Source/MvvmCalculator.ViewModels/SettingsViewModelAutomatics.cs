﻿using GalaSoft.MvvmLight.Messaging;
using MvvmCalculator.Core.Messages;

namespace MvvmCalculator.ViewModels
{
    public class SettingsViewModelSettingsAutomatic : SettingsViewModelBase
    {
        public SettingsViewModelSettingsAutomatic()
        {
            this.CreateRegistrations();
        }

        private void CreateRegistrations()
        {
            Messenger.Default.Register<SaveSettingsMessage>(this, this.SaveSettingsHandler);
        }

        private void SaveSettingsHandler(SaveSettingsMessage message)
        {
            message.Data.VibrateOn = this.VibrateOn;

            Messenger.Default.Send(new SavingSettingsMessage(message.Data));
        }
    }
}