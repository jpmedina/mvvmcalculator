﻿using MvvmCalculator.Core.Models;

namespace MvvmCalculator.Core.Messages
{
    public class SaveSettingsMessage : MessageBase<SavedSettingsModel>
    {
        public SaveSettingsMessage(SavedSettingsModel model) : base(model)
        {
            
        }
    }
}