﻿using System;
using System.IO.IsolatedStorage;
using GalaSoft.MvvmLight.Messaging;
using MvvmCalculator.Core.Messages;

namespace MvvmCalculator.Core.Models
{
    public class SettingsModel
    {
        private IsolatedStorageSettings Settings
        {
            get { return IsolatedStorageSettings.ApplicationSettings; }
        }

        public SettingsModel()
        {
            Messenger.Default.Register<SavingSettingsMessage>(
                this, this.SavingSettingsHandler);
            Messenger.Default.Register<RestoreSettingsMessage>(
                this, this.RestoreSettingsHandler);

            if( !Settings.Contains("Settings") )
            {
                Settings.Add("Settings", new SavedSettingsModel());
            }
        }

        /// <summary>
        /// 
        /// Save/updates settings.
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void SavingSettingsHandler(SavingSettingsMessage message)
        {
            bool saved = false;

            try
            {
                Settings["Settings"] = message.Data;
                saved = true;
            }
            catch (Exception e)
            {
                
            }

            Messenger.Default.Send(new SettingsSavedMessage(saved));
        }

        /// <summary>
        /// 
        /// Sends the requester the current saved settings.
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void RestoreSettingsHandler(RestoreSettingsMessage message)
        {
            var model = new SavedSettingsModel();

            if( Settings.Contains("Settings") )
            {
                model = (SavedSettingsModel) Settings["Settings"];
            }

            Messenger.Default.Send(new SettingsRestoredMessage(model));
        }
    }
}