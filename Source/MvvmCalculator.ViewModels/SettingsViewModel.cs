﻿using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Windows;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator.ViewModels
{
    public class SettingsViewModel : SettingsViewModelBase
    {
        private readonly CalculatorViewModelTraditional _calculatorViewModel;

        private IsolatedStorageSettings Settings
        {
            get { return IsolatedStorageSettings.ApplicationSettings; }
        }

        public SettingsViewModel(CalculatorViewModelTraditional calculatorViewModel)
        {
            _calculatorViewModel = calculatorViewModel;

            this.ShowDialogVisibility = Visibility.Collapsed;

            if(!Settings.Contains("Settings"))
            {
                InternalSaveSettingsHandler();
            }
            else
            {
                InternalRestoreSettingsHandler();
            }
        }

        private void InternalSaveSettingsHandler()
        {
            if(!Settings.Contains("Settings"))
            {
                Settings.Add("Settings", new SavedSettingsModel());
                Settings.Save();
            }

            var settings = (SavedSettingsModel)Settings["Settings"];

            var copy = new List<CalculationResult>(_calculatorViewModel.Calculations);

            settings.Calculations = copy;
            settings.VibrateOn = this.VibrateOn;

            Settings["Settings"] = settings;
            Settings.Save();

            this.SettingsSavedStateText = "Saved!!!!";
        }

        protected override void SaveSettingsHandler()
        {
            InternalSaveSettingsHandler();
        }

        private void InternalRestoreSettingsHandler()
        {
            var settings = (SavedSettingsModel)Settings["Settings"];

            this.VibrateOn = settings.VibrateOn;

            if(settings == null || settings.Calculations == null)
            {
                return;
            }

            _calculatorViewModel.Calculations.Clear();

            foreach(var cals in settings.Calculations)
            {
                _calculatorViewModel.Calculations.Add(cals);
            }

            this.SettingsRestoredStateText = "Restored!!!!";
        }

        protected override void RestoreSettingsHandler()
        {
            InternalRestoreSettingsHandler();
        }
    }
}