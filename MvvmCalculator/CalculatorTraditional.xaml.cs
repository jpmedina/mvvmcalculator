﻿using System;
using System.Windows;
using System.Windows.Media;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator
{
    public partial class CalculatorTraditional
    {
        private readonly Brush _leftDefaultBrush;
        private readonly Brush _rightDefaultBrush;

        public CalculatorTraditional()
        {
            InitializeComponent();

            _leftDefaultBrush = this.LeftNumberValidationTextBox.Foreground;
            _rightDefaultBrush = this.RightNumberValidationTextBox.Foreground;
        }

        private void OperationAddButton_Click(object sender, RoutedEventArgs e)
        {
            this.CalculatorResult.Text =
                this.CalculatedResults(
                    (l, r) => l.Number + r.Number);
        }

        private void OperationSubtractButton_Click(object sender, RoutedEventArgs e)
        {
            this.CalculatorResult.Text =
                this.CalculatedResults(
                    (l, r) => l.Number - r.Number);
        }

        private void OperationMultiplyButton_Click(object sender, RoutedEventArgs e)
        {
            this.CalculatorResult.Text =
                this.CalculatedResults(
                    (l, r) => l.Number * r.Number);
        }

        private void OperationDivideButton_Click(object sender, RoutedEventArgs e)
        {
            this.CalculatorResult.Text =
                this.CalculatedResults(
                    (l, r) => l.Number / r.Number);
        }

        private NumberConversionResult ConvertTextToNumber(string text)
        {
            double number;

            return double.TryParse(text, out number) ?
                new NumberConversionResult(true, number) :
                new NumberConversionResult(false, -1);
        }

        private NumberConversionResult ValidateLeftTextbox()
        {
            var left = this.ConvertTextToNumber(this.LeftNumberTextBox.Text);

            if (!left.IsNumber)
            {
                this.LeftNumberValidationTextBox.Foreground = new SolidColorBrush(Colors.Red);
                this.LeftNumberValidationTextBox.Text = "Left Number Must Be A Valid Integer";
            }
            else
            {
                this.LeftNumberValidationTextBox.Foreground = _leftDefaultBrush;
                this.LeftNumberValidationTextBox.Text = "";
            }

            return left;
        }

        private NumberConversionResult ValidateRightTextbox()
        {
            var right = this.ConvertTextToNumber(this.RightNumberTextBox.Text);

            if (!right.IsNumber)
            {
                this.RightNumberValidationTextBox.Foreground = new SolidColorBrush(Colors.Red);
                this.RightNumberValidationTextBox.Text = "Right Number Must Be A Valid Integer";
            }
            else
            {
                this.RightNumberValidationTextBox.Foreground = _rightDefaultBrush;
                this.RightNumberValidationTextBox.Text = "";
            }

            return right;
        }

        private string CalculatedResults(
            Func<NumberConversionResult, NumberConversionResult, double> result)
        {
            var left = this.ValidateLeftTextbox();
            var right = this.ValidateRightTextbox();

            if (left.IsNumber && right.IsNumber)
            {
                return string.Format("{0:0.00}", result(left, right));

            }

            return string.Empty;
        }

        private void ResultClearButton_Click(object sender, RoutedEventArgs e)
        {
            this.CalculatorResult.Text = "";
        }
    }
}