﻿using MvvmCalculator.Core.Models;

namespace MvvmCalculator.Core.Messages
{
    public class SavingSettingsMessage : MessageBase<SavedSettingsModel>
    {
        public SavingSettingsMessage(SavedSettingsModel model) : base(model)
        {

        }
    }
}