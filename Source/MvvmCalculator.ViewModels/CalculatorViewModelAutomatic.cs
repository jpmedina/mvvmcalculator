﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using MvvmCalculator.Core.Messages;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator.ViewModels
{
    public class CalculatorViewModelAutomatic : CalculatorViewModelBase
    {
        public CalculatorViewModelAutomatic()
        {
            this.CreateRegistrations();
        }

        private void CreateRegistrations()
        {
            Messenger.Default.Register<SaveSettingsMessage>(this, this.SaveSettingsHandler);
            Messenger.Default.Register<SettingsRestoredMessage>(this, this.SettingsRestoredHandler);
        }

        #region Handlers

        private void SaveSettingsHandler(SaveSettingsMessage message)
        {
            var list = new List<CalculationResult>(base.Calculations);

            message.Data.Calculations = list;

            Messenger.Default.Send(new SavingSettingsMessage(message.Data));
        }

        private void SettingsRestoredHandler(SettingsRestoredMessage message)
        {
            if( message == null || message.Data.Calculations == null || message.Data.Calculations.Count == 0 )
            {
                return;
            }

            base.Calculations.Clear();
            
            foreach(var result in message.Data.Calculations)
            {
                base.Calculations.Add(result);
            }
        }

        private void SaveSettingsHandler()
        {
            var list = new List<CalculationResult>(base.Calculations);

            Messenger.Default.Send(
                new SaveSettingsMessage(
                    new SavedSettingsModel
                    {
                        Calculations = list
                    }));
        }

        #endregion

        /// <summary>
        /// 
        /// Every time a result is created save the new list.
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="result"></param>
        /// <param name="operation"></param>
        protected override void AddCalculationResult(double left, double right, double result, string operation)
        {
            base.AddCalculationResult(left, right, result, operation);

            SaveSettingsHandler();
        }
    }
}