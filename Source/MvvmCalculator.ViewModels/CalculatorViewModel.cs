﻿using System;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator.ViewModels
{
    public class CalculatorViewModel : ViewModelBase
    {
        private string _leftValidationMessage;
        private string _rightValidationMessage;

        private string _leftNumber;
        private string _rightNumber;
        private string _calculationResult;

        private SolidColorBrush DEFAULT_MESSAGE_COLOR = new SolidColorBrush(Colors.Black);
        private SolidColorBrush _leftColorBrush;
        private SolidColorBrush _rightColorBrush;

        public SolidColorBrush LeftMessageColor
        {
            get { return _leftColorBrush; }
            set
            {
                _leftColorBrush = value;
                base.RaisePropertyChanged(() => this.LeftMessageColor);
            }
        }

        public SolidColorBrush RightMessageColor
        {
            get { return _rightColorBrush; }
            set
            {
                _rightColorBrush = value;
                base.RaisePropertyChanged(() => this.RightMessageColor);
            }
        }

        public string LeftNumberValidationMessage
        {

            get { return _leftValidationMessage; }
            set
            {
                _leftValidationMessage = value;
                base.RaisePropertyChanged(() => this.LeftNumberValidationMessage);
            }
        }

        public string RightNumberValidationMessage
        {
            get { return _rightValidationMessage; }
            set
            {
                _rightValidationMessage = value;
                base.RaisePropertyChanged(() => this.RightNumberValidationMessage);
            }
        }

        public string LeftNumber
        {
            get { return _leftNumber; }
            set
            {
                _leftNumber = value;
                base.RaisePropertyChanged(() => this.LeftNumber);
            }
        }

        public string RightNumber
        {
            get { return _rightNumber; }
            set
            {
                _rightNumber = value;
                base.RaisePropertyChanged(() => this.RightNumber);
            }
        }

        public string CalculationResult
        {
            get { return _calculationResult; }
            set
            {
                _calculationResult = value;
                base.RaisePropertyChanged(() => this.CalculationResult);
            }
        }

        public RelayCommand OperationAddCommand { get; private set; }
        public RelayCommand OperationSubtractionCommand { get; private set; }
        public RelayCommand OperationMultiplicationCommand { get; private set; }
        public RelayCommand OperationDivisionCommand { get; private set; }
        public RelayCommand ResultResetCommand { get; private set; }

        public CalculatorViewModel()
        {
            this.OperationAddCommand = new RelayCommand(this.OperationAddHandler);
            this.OperationSubtractionCommand = new RelayCommand(this.OperationSubtractionHandler);
            this.OperationMultiplicationCommand = new RelayCommand(this.OperationMultiplicationHandler);
            this.OperationDivisionCommand = new RelayCommand(this.OperationDivisionHandler);
            this.ResultResetCommand = new RelayCommand(this.ResultResetHandler);

            this.CalculationResult = "0.00";
        }

        #region Handlers

        public void OperationAddHandler()
        {
            this.CalculationResult =
                this.CalculatedResults(
                    (l, r) => l.Number + r.Number);
        }

        public void OperationSubtractionHandler()
        {
            this.CalculationResult =
                this.CalculatedResults(
                    (l, r) => l.Number - r.Number);
        }

        public void OperationMultiplicationHandler()
        {
            this.CalculationResult =
                this.CalculatedResults(
                    (l, r) => l.Number * r.Number);
        }

        public void OperationDivisionHandler()
        {
            this.CalculationResult =
                this.CalculatedResults(
                    (l, r) => l.Number / r.Number);
        }

        public void ResultResetHandler()
        {
            this.CalculationResult = string.Empty;
        }

        #endregion

        private NumberConversionResult ConvertTextToNumber(string text)
        {
            double number;

            return double.TryParse(text, out number) ?
                new NumberConversionResult(true, number) :
                new NumberConversionResult(false, -1);
        }

        private NumberConversionResult ValidateLeftTextbox()
        {
            var left = this.ConvertTextToNumber(this.LeftNumber);

            if (!left.IsNumber)
            {
                this.LeftMessageColor = new SolidColorBrush(Colors.Red);
                this.LeftNumberValidationMessage = "Left Number Must Be A Valid Integer";
            }
            else
            {
                this.LeftMessageColor = DEFAULT_MESSAGE_COLOR;
                this.LeftNumberValidationMessage = "";
            }

            return left;
        }

        private NumberConversionResult ValidateRightTextbox()
        {
            var right = this.ConvertTextToNumber(this.RightNumber);

            if (!right.IsNumber)
            {
                this.RightMessageColor = new SolidColorBrush(Colors.Red);
                this.RightNumberValidationMessage = "Right Number Must Be A Valid Integer";
            }
            else
            {
                this.RightMessageColor = DEFAULT_MESSAGE_COLOR;
                this.RightNumberValidationMessage = "";
            }

            return right;
        }

        private string CalculatedResults(
            Func<NumberConversionResult, NumberConversionResult, double> result)
        {
            var left = this.ValidateLeftTextbox();
            var right = this.ValidateRightTextbox();

            if (left.IsNumber && right.IsNumber)
            {
                return string.Format("{0:0.00}", result(left, right));

            }

            return string.Empty;
        }
    }
}