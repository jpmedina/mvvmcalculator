﻿using MvvmCalculator.Core.Models;
using MvvmCalculator.ViewModels;
using SimpleInjector;

namespace MvvmCalculator.Service
{
    public class Factory
    {
        private static readonly Factory _factory = new Factory();
        private static readonly Container _container;

        static Factory()
        {
            _container = new Container();

            Register(_container);

            //HACK, ideally this will be a dependency but for now it only
            //needs to be live in order to handle messages
            var settingsModel = _container.GetInstance<SettingsModel>();
        }

        private Factory()
        {
            
        }

        public static Factory Instance 
        {
            get
            {
                return _factory;
            }
        }

        private static void Register(Container container)
        {
            //
            // Don't use Lifestyle.Singleton if the class has
            // any state.  These don't have state  so there 
            // should not be any side-effects.  However, if memory
            // becomes an issue, use the default transient.
            //
            // ViewModels
            //
            container.Register<CalculatorViewModelTraditional>(Lifestyle.Singleton);
            container.Register<CalculatorViewModelRefactored>(Lifestyle.Singleton);
            container.Register<CalculatorViewModelAutomatic>(Lifestyle.Singleton);

            container.Register<SettingsViewModelSettingsAutomatic>(Lifestyle.Singleton);
            container.Register<SettingsViewModelRefactored>(Lifestyle.Singleton);
            container.Register<SettingsViewModel>(Lifestyle.Singleton);
            container.Register<SettingsModel>(Lifestyle.Singleton);
        }

        public T GetInstance<T>() where T : class
        {
            return _container.GetInstance<T>();
        }
    }
}