﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator.ViewModels
{
    public class CalculatorViewModelBase : ViewModelBase
    {
        private string _leftValidationMessage;
        private string _rightValidationMessage;

        private string _leftNumber;
        private string _rightNumber;

        /// <summary>
        /// 
        /// Used to keep track of all of the calculations that were computed
        /// 
        /// </summary>
        private ObservableCollection<CalculationResult> _calculations =
            new ObservableCollection<CalculationResult>();

        private readonly SolidColorBrush DEFAULT_MESSAGE_COLOR = new SolidColorBrush(Colors.Black);
        private SolidColorBrush _leftColorBrush;
        private SolidColorBrush _rightColorBrush;

        #region Properties

        public RelayCommand OperationAddCommand { get; private set; }
        public RelayCommand OperationSubtractionCommand { get; private set; }
        public RelayCommand OperationMultiplicationCommand { get; private set; }
        public RelayCommand OperationDivisionCommand { get; private set; }
        public RelayCommand ResultResetCommand { get; private set; }
        public RelayCommand<CalculationResult> OperationReloadCommand { get; private set; }
        public RelayCommand<RoutedEventArgs> CalculationTextBoxGotFocusCommand { get; private set; }

        public SolidColorBrush LeftMessageColor
        {
            get { return _leftColorBrush; }
            set
            {
                _leftColorBrush = value;
                base.RaisePropertyChanged(() => this.LeftMessageColor);
            }
        }

        public SolidColorBrush RightMessageColor
        {
            get { return _rightColorBrush; }
            set
            {
                _rightColorBrush = value;
                base.RaisePropertyChanged(() => this.RightMessageColor);
            }
        }

        public string LeftNumberValidationMessage
        {

            get { return _leftValidationMessage; }
            set
            {
                _leftValidationMessage = value;
                base.RaisePropertyChanged(() => this.LeftNumberValidationMessage);
            }
        }

        public string RightNumberValidationMessage
        {
            get { return _rightValidationMessage; }
            set
            {
                _rightValidationMessage = value;
                base.RaisePropertyChanged(() => this.RightNumberValidationMessage);
            }
        }

        public string LeftNumber
        {
            get { return _leftNumber; }
            set
            {
                _leftNumber = value;
                base.RaisePropertyChanged(() => this.LeftNumber);
            }
        }

        public string RightNumber
        {
            get { return _rightNumber; }
            set
            {
                _rightNumber = value;
                base.RaisePropertyChanged(() => this.RightNumber);
            }
        }

        public ObservableCollection<CalculationResult> Calculations
        {
            get { return _calculations; }
            set
            {
                _calculations = value;
                base.RaisePropertyChanged(() => this.Calculations);
            }
        }

        #endregion

        protected CalculatorViewModelBase()
        {
            this.OperationAddCommand = new RelayCommand(this.OperationAddHandler);
            this.OperationSubtractionCommand = new RelayCommand(this.OperationSubtractionHandler);
            this.OperationMultiplicationCommand = new RelayCommand(this.OperationMultiplicationHandler);
            this.OperationDivisionCommand = new RelayCommand(this.OperationDivisionHandler);
            this.ResultResetCommand = new RelayCommand(this.ResultResetHandler);
            this.OperationReloadCommand = new RelayCommand<CalculationResult>(this.OperationReloadHandler);
            this.CalculationTextBoxGotFocusCommand = new RelayCommand<RoutedEventArgs>(this.CalculationTextBoxGotFocus);
        }


        private void OperationAddHandler()
        {
            this.CalculatedResults(
                (l, r) =>
                {
                    var result = l.Number + r.Number;

                    AddCalculationResult(l.Number, r.Number, result, "+");

                    return result;
                });
        }

        private void OperationSubtractionHandler()
        {
            this.CalculatedResults(
                (l, r) =>
                {
                    var result = l.Number - r.Number;

                    AddCalculationResult(l.Number, r.Number, result, "-");

                    return result;
                });
        }

        private void OperationMultiplicationHandler()
        {
            this.CalculatedResults(
                (l, r) =>
                {
                    var result = l.Number * r.Number;

                    AddCalculationResult(l.Number, r.Number, result, "*");

                    return result;
                });
        }

        private void OperationDivisionHandler()
        {
            this.CalculatedResults(
                (l, r) =>
                {
                    var result = l.Number / r.Number;

                    AddCalculationResult(l.Number, r.Number, result, "/");

                    return result;
                });
        }

        private void ResultResetHandler()
        {
            _calculations.Clear();
        }

        private void OperationReloadHandler(CalculationResult oldResult)
        {
            this.LeftNumber = oldResult.Left.ToString();
            this.RightNumber = oldResult.Right.ToString();
        }

        private void CalculationTextBoxGotFocus(RoutedEventArgs e)
        {
            var textbox = e.OriginalSource as TextBox;

            if(textbox == null)
            {
                return;
            }

            textbox.SelectAll();
        }

        #region Private Helpers

        private string CalculatedResults(
            Func<NumberConversionResult, NumberConversionResult, double> result)
        {
            var left = this.ValidateLeftTextbox();
            var right = this.ValidateRightTextbox();

            if(left.IsNumber && right.IsNumber)
            {
                return string.Format("{0:0.00}", result(left, right));
            }

            return string.Empty;
        }

        protected virtual void AddCalculationResult(double left, double right, double result, string operation)
        {
            var calculationResult = new CalculationResult(left, right, result, operation);

            _calculations.Add(calculationResult);
        }

        private NumberConversionResult ValidateLeftTextbox()
        {
            var left = this.ConvertTextToNumber(this.LeftNumber);

            if(!left.IsNumber)
            {
                this.LeftMessageColor = new SolidColorBrush(Colors.Red);
                this.LeftNumberValidationMessage = "Left Number Must Be A Valid Integer";
            }
            else
            {
                this.LeftMessageColor = DEFAULT_MESSAGE_COLOR;
                this.LeftNumberValidationMessage = "";
            }

            return left;
        }

        private NumberConversionResult ValidateRightTextbox()
        {
            var right = this.ConvertTextToNumber(this.RightNumber);

            if(!right.IsNumber)
            {
                this.RightMessageColor = new SolidColorBrush(Colors.Red);
                this.RightNumberValidationMessage = "Right Number Must Be A Valid Integer";
            }
            else
            {
                this.RightMessageColor = DEFAULT_MESSAGE_COLOR;
                this.RightNumberValidationMessage = "";
            }

            return right;
        }

        private NumberConversionResult ConvertTextToNumber(string text)
        {
            double number;

            return double.TryParse(text, out number) ?
                new NumberConversionResult(true, number) :
                new NumberConversionResult(false, -1);
        }

        #endregion
    }
}