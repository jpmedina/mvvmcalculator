﻿using System.Collections.ObjectModel;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator.ViewModels
{
    public interface ICalculatorViewModel
    {
        ObservableCollection<CalculationResult> Calculations { get; set; }
    }
}