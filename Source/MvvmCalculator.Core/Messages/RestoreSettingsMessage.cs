﻿namespace MvvmCalculator.Core.Messages
{
    public class RestoreSettingsMessage : MessageBase<bool>
    {
        public RestoreSettingsMessage(bool restore) : base(restore)
        {
            
        }
    }
}