﻿namespace MvvmCalculator.Core
{
    public abstract class MessageBase<T>
    {
        public T Data { get; private set; }
        public object Sender { get; private set; }
        public string Key { get; private set; }

        protected MessageBase(T data = default(T), object sender = null, string key = "")
        {
            this.Data = data;
            this.Sender = sender;
            this.Key = key;
        }
    }
}