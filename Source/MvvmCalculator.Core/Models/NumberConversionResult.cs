﻿namespace MvvmCalculator.Core.Models
{
    public struct NumberConversionResult
    {
        public bool IsNumber { get; private set; }
        public double Number { get; private set; }

        public NumberConversionResult(bool isNumber, double number) : this()
        {
            this.IsNumber = isNumber;
            this.Number = number;
        }
    }
}