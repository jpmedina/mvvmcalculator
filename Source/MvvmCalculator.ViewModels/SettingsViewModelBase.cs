﻿using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MvvmCalculator.Core.Messages;
using MvvmCalculator.Core.Models;

namespace MvvmCalculator.ViewModels
{
    public class SettingsViewModelBase : ViewModelBase
    {
        private Visibility _showDialogVisibility;
        private string _settingsSavedStateText;
        private string _settingsRestoredStateText;
        private bool _vibrateOn;

        public RelayCommand ToggleDialogCommand { get; private set; }
        public RelayCommand SaveSettingsCommand { get; private set; }
        public RelayCommand RestoreSettingsCommand { get; private set; }
        

        public Visibility ShowDialogVisibility
        {
            get { return _showDialogVisibility; }
            set
            {
                _showDialogVisibility = value;
                base.RaisePropertyChanged(() => this.ShowDialogVisibility);
            }
        }

        public bool VibrateOn
        {
            get { return _vibrateOn; }
            set
            {
                _vibrateOn = value;
                base.RaisePropertyChanged(() => this.VibrateOn);
            }
        }

        public string SettingsSavedStateText
        {
            get { return _settingsSavedStateText; }
            set
            {
                _settingsSavedStateText = value;
                base.RaisePropertyChanged(() => this.SettingsSavedStateText);
            }
        }

        public string SettingsRestoredStateText
        {
            get { return _settingsRestoredStateText; }
            set
            {
                _settingsRestoredStateText = value;
                base.RaisePropertyChanged(() => this.SettingsRestoredStateText);
            }
        }

        public SettingsViewModelBase()
        {
            this.ShowDialogVisibility = Visibility.Collapsed;

            this.ToggleDialogCommand = new RelayCommand(this.ToggleDialogHandler);
            this.SaveSettingsCommand = new RelayCommand(this.SaveSettingsHandler);
            this.RestoreSettingsCommand = new RelayCommand(this.RestoreSettingsHandler);

            this.SettingsRestoredStateText = "Not Restored";
            this.SettingsSavedStateText = "Not Saved";

            this.CreateRegistrations();
        }

        private void CreateRegistrations()
        {
            Messenger.Default.Register<SettingsRestoredMessage>(this, this.SettingsRestoredHandler);
            Messenger.Default.Register<SettingsSavedMessage>(this, this.SettingsSavedHandler);
        }

        private void ToggleDialogHandler()
        {
            if( this.ShowDialogVisibility == Visibility.Visible )
            {
                this.ShowDialogVisibility = Visibility.Collapsed;
                this.SettingsRestoredStateText = "Not Restored";
                this.SettingsSavedStateText = "Not Saved";
            }
            else if( this.ShowDialogVisibility == Visibility.Collapsed )
            {
                this.ShowDialogVisibility = Visibility.Visible;
            }
        }

        private void SettingsSavedHandler(SettingsSavedMessage mesage)
        {
            this.SettingsSavedStateText = "Saved!!!!";
        }

        private void SettingsRestoredHandler(SettingsRestoredMessage message)
        {
            this.VibrateOn = message.Data.VibrateOn;
            this.SettingsRestoredStateText = "Restored!!!!";
        }

        protected virtual void SaveSettingsHandler()
        {
            Messenger.Default.Send(
                new SaveSettingsMessage(
                    new SavedSettingsModel
                    {
                        VibrateOn = this.VibrateOn
                    }));
        }

        protected virtual void RestoreSettingsHandler()
        {
            Messenger.Default.Send(new RestoreSettingsMessage(true));
        }
    }
}