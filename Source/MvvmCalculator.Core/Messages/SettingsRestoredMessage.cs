﻿using MvvmCalculator.Core.Models;

namespace MvvmCalculator.Core.Messages
{
    public class SettingsRestoredMessage : MessageBase<SavedSettingsModel>
    {
        public SettingsRestoredMessage(SavedSettingsModel model) : base(model)
        {
            
        }
    }
}