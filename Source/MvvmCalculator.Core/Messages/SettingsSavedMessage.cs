﻿namespace MvvmCalculator.Core.Messages
{
    public class SettingsSavedMessage : MessageBase<bool>
    {
        public SettingsSavedMessage(bool saved) : base(saved)
        {
            
        }
    }
}