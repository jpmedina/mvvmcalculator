﻿using System.Collections.Generic;

namespace MvvmCalculator.Core.Models
{
    public class SavedSettingsModel
    {
        public bool VibrateOn { get; set; }
        public List<CalculationResult> Calculations { get; set; }
    }
}